# Color Your World (Zach's version)

## changes I made

- removed self-hosted fonts and used external ones instead ([Open Sans](https://www.opensans.com/) and [Oswald](https://github.com/vernnobile/OswaldFont) hosted by [Brick](https://brick.im/))
- added icons for the contact page
  - the iNaturalist logo is from [academicicons](https://github.com/jpswalsh/academicons)
  - the Devpost logo is from [this Devpost blog post](https://devpost.com/software/devpost-follow-buttons-social-icons).
- enabled lowercase letters in menu
- added an underline animation based on the one used by [Farhan Bin Amin](https://farhan2077.github.io/)
- added ellipses animation
- added a favicon that I generated with [favicon.io](https://favicon.io)
- added [GoatCounter](https://www.goatcounter.com/) analytics
